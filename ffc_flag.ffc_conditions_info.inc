<?php

/**
 * @file
 * Hooks for Field formatter conditions: Flag.
 */

/**
 * Makes the show_if_flagged condition available in Field UI.
 *
 * Implements hook_ffc_conditions_info().
 */
function ffc_flag_ffc_conditions_info() {
  $conditions = array();
  $conditions['show_if_flagged'] = array(
    'title' => t('Show field depending on if entity is flagged'),
  );
  return $conditions;
}

/**
 * Config subform for setting flag-based conditions.
 *
 * Seen on field UI form when the flag condition is selected.
 *
 * Implements ffc_condition_form_CONDITION().
 */
function ffc_condition_form_show_if_flagged($context, $configuration) {
  $form = array();

  // @see _flag_rules_get_options()
  $flags = flag_get_flags();
  $options = array();
  // Filter agressively to ensure the flag rules are appropriate.
  foreach ($flags as $flag) {
    if (!isset($context['bundle']) || $flag->entity_type == $context['entity_type']) {
      if (!isset($context['bundle']) || in_array($context['bundle'], $flag->types)) {
        $options[$flag->name] = $flag->get_title();
      }
    }
  }

  $form['flag'] = array(
    '#type' => 'select',
    '#title' => t('Flag'),
    '#options' => $options,
    '#default_value' => isset($configuration['flag']) ? $configuration['flag'] : '',
  );
  $form['flag_state'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show if flag state is on (hide otherwise)'),
    '#default_value' => isset($configuration['flag_state']) ? $configuration['flag_state'] : TRUE,
  );

  return $form;
}
